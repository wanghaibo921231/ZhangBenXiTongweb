let extra_env = {};
if (process.env.NODE_ENV === "production") {
  extra_env = {
    css: {
      extract: true
    },
    assetsDir: "assets",
    outputDir: "seller",
    publicPath: "/seller"
  };
}
module.exports = {
  devServer: {
    proxy: {
      "/seller": {
        target: process.env.PROXY_URL,
        ws: false,
        changeOrigin: true
      }
    }
  },
  productionSourceMap: false,
  ...extra_env
};
