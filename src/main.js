import Vue from "vue";
import App from "./App.vue";
import router from "./router";
//引入antd
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import "./themes/index.less";
import UDialog from "./components/Dialog/Dialog.vue";
import common from "@/utils/common";
import Viser from "viser-vue";
import store from "./models";
import "./utils/mock";
//开启debug
Vue.config.productionTip = false;
Vue.use(Antd);
Vue.use(Viser);
Vue.component("u-dialog", UDialog);
Vue.prototype.$common = common;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
