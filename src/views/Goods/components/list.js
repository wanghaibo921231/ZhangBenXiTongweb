const columns = [
  {
    title: "商品名",
    width: 150,
    dataIndex: "name",
    key: "name"
  },
  {
    title: "类型",
    dataIndex: "goods_type.name",
    key: "goods_type.name",
    width: 100
  },
  { title: "库存", dataIndex: "stock", key: "stock", width: 80 },
  {
    title: "最大价格",
    dataIndex: "max_price",
    key: "max_price",
    width: 100,
    scopedSlots: { customRender: "max_price" }
  },
  {
    title: "最低价格",
    dataIndex: "min_price",
    key: "min_price",
    width: 100,
    scopedSlots: { customRender: "min_price" }
  },
  { title: "销量", dataIndex: "sale_num", key: "sale_num", width: 80 },
  {
    title: "状态",
    dataIndex: "status",
    key: "status",
    width: 150,
    scopedSlots: { customRender: "status" }
  },
  { title: "创建时间", dataIndex: "created_at", key: "created_at", width: 200 },
  {
    title: "操作",
    key: "operation",
    width: 200,
    scopedSlots: { customRender: "action" }
  }
];
export default columns;
