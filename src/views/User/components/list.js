const columns = [
  {
    title: "昵称",
    width: 150,
    dataIndex: "nickname",
    key: "nickname"
  },
  { title: "手机号", dataIndex: "mobile", key: "mobile", width: 200 },
  { title: "备注", dataIndex: "desc", key: "desc", width: 200 },
  { title: "等级", dataIndex: "level", key: "level", width: 150 },
  {
    title: "余额",
    dataIndex: "balance",
    key: "balance",
    width: 150,
    scopedSlots: { customRender: "balance" }
  },
  {
    title: "状态",
    dataIndex: "status",
    key: "status",
    width: 150,
    scopedSlots: { customRender: "status" }
  },
  { title: "创建时间", dataIndex: "created_at", key: "created_at", width: 200 },
  {
    title: "操作",
    key: "operation",
    width: 200,
    scopedSlots: { customRender: "action" }
  }
];
export default columns;
