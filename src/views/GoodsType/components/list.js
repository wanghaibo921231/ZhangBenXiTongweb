const columns = [
  {
    title: "排序",
    dataIndex: "sort",
    key: "sort",
    width: "10%"
  },
  {
    title: "分类名称",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "状态",
    dataIndex: "status",
    key: "status",
    scopedSlots: { customRender: "status" }
  },
  {
    title: "操作",
    key: "operation",
    width: 200,
    scopedSlots: { customRender: "action" }
  }
];
export default columns;
