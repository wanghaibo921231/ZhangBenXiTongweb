const columns = [
  {
    title: "",
    dataIndex: "is_income",
    key: "is_income",
    width: 10,
    scopedSlots: { customRender: "is_income" }
  },
  {
    title: "金额",
    dataIndex: "amount",
    key: "amount",
    width: 150,
    scopedSlots: { customRender: "amount" }
  },
  {
    title: "类型",
    dataIndex: "type_name",
    key: "type_name",
    width: 100
  },
  { title: "日期", dataIndex: "generated_at", key: "generated_at", width: 200 },
  {
    title: "备注",
    dataIndex: "desc",
    key: "desc"
  },
  {
    title: "操作",
    key: "operation",
    width: 200,
    scopedSlots: { customRender: "action" }
  }
];
export default columns;
