import Vue from "vue";
import Router from "vue-router";
import Dashboard from "./views/Dashboard/Index.vue";
import Layout from "./components/Layout/Layout.vue";
Vue.use(Router);
const routes = [
  {
    path: "/",
    name: "Index",
    redirect: "/login",
    meta: {
      breadcrumbName: "首页"
    },
    component: Layout,
    children: [
      {
        path: "/index",
        name: "Dashboard",
        meta: {
          breadcrumbName: "仪表盘"
        },
        component: Dashboard
      },
      {
        path: "/users",
        redirect: "/users/index",
        name: "Users",
        meta: {
          breadcrumbName: "用户管理"
        },
        component: () => import("@/components/Layout/ContentLayout"),
        children: [
          {
            path: "/users/index",
            name: "UserList",
            meta: {
              breadcrumbName: "用户列表"
            },
            component: () => import("@/views/User/List")
          }
        ]
      },
      {
        path: "/goods",
        redirect: "/goods/index",
        name: "Goods",
        meta: {
          breadcrumbName: "商品管理"
        },
        component: () => import("@/components/Layout/ContentLayout"),
        children: [
          {
            path: "/goods/index",
            name: "GoodsList",
            meta: {
              breadcrumbName: "商品列表"
            },
            component: () => import("@/views/Goods/List")
          },
          {
            path: "/goods/create",
            name: "GoodsCreate",
            meta: {
              breadcrumbName: "添加商品"
            },
            component: () => import("@/views/Goods/Create")
          },
          {
            path: "/goods/edit/:id",
            name: "GoodsEdit",
            meta: {
              breadcrumbName: "编辑商品"
            },
            component: () => import("@/views/Goods/Edit")
          },
          {
            path: "/goods/goodsType",
            name: "GoodsTypeList",
            meta: {
              breadcrumbName: "分类管理"
            },
            component: () => import("@/views/GoodsType/List")
          }
        ]
      },
      {
        path: "/account",
        redirect: "/account/setting",
        name: "Account",
        meta: {
          breadcrumbName: "个人页"
        },
        component: () => import("@/components/Layout/ContentLayout"),
        children: [
          {
            path: "/account/setting",
            name: "AccountSetting",
            meta: {
              breadcrumbName: "个人设置"
            },
            component: () => import("@/views/Account/Setting")
          }
        ]
      },
      {
        path: "/finance",
        redirect: "/finance/index",
        name: "Finance",
        meta: {
          breadcrumbName: "财务管理"
        },
        component: () => import("@/components/Layout/ContentLayout"),
        children: [
          {
            path: "/finance/index",
            name: "FinanceList",
            meta: {
              breadcrumbName: "财务列表"
            },
            component: () => import("@/views/Finance/List")
          }
        ]
      }
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login/Login")
  }
];
export default new Router({
  routes
});
