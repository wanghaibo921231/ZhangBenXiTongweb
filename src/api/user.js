import request from "@/utils/request";

export function fetchList(params) {
  return request({
    url: "/user",
    method: "get",
    params
  });
}
export function fetchInfo(id) {
  return request({
    url: "/user/" + id,
    method: "get"
  });
}
export function setStatus(id) {
  return request({
    url: "/user/" + id + "/setStatus",
    method: "patch"
  });
}

export function createUser(data) {
  return request({
    url: "/user",
    method: "post",
    data
  });
}
export function updateUser(data, id) {
  return request({
    url: "/user/" + id,
    method: "post",
    data
  });
}
export function deleteUser(id) {
  return request({
    url: "/user/" + id,
    method: "delete"
  });
}
export function topUp(data, user_id) {
  return request({
    url: "/user/" + user_id + "/topUp",
    method: "post",
    data
  });
}

export function consume(data, user_id) {
  return request({
    url: "/user/" + user_id + "/consume",
    method: "post",
    data
  });
}
export function transaction(params) {
  return request({
    url: "/user/transaction",
    method: "get",
    params
  });
}
