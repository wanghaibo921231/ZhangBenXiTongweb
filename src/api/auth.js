import request from "@/utils/request";

export function login(data) {
  return request({
    url: "/auth/login",
    method: "post",
    data
  });
}
export function fetchInfo() {
  return request({
    url: "/auth/info",
    method: "get"
  });
}
export function logout() {
  return request({
    url: "/auth/logout",
    method: "post"
  });
}
export function updateInfo(data) {
  return request({
    url: "/auth/setInfo",
    method: "post",
    data
  });
}
export function changePwd(data) {
  return request({
    url: "/auth/changePassword",
    method: "patch",
    data
  });
}
