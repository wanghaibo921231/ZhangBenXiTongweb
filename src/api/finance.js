import request from "@/utils/request";

export function fetchList(params) {
  return request({
    url: "/finance",
    method: "get",
    params
  });
}
export function fetchInfo(id) {
  return request({
    url: "/finance/" + id,
    method: "get"
  });
}

export function createFinance(data) {
  return request({
    url: "/finance",
    method: "post",
    data
  });
}
export function deleteFinance(id) {
  return request({
    url: "/finance/" + id,
    method: "delete"
  });
}
export function fetchType() {
  return request({
    url: "/finance/fetchType",
    method: "get"
  });
}
