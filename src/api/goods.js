import request from "@/utils/request";

export function fetchList(params) {
  return request({
    url: "/goods",
    method: "get",
    params
  });
}
export function fetchInfo(id) {
  return request({
    url: "/goods/" + id,
    method: "get"
  });
}
export function setStatus(id) {
  return request({
    url: "/goods/" + id + "/setStatus",
    method: "patch"
  });
}

export function createGoods(data) {
  return request({
    url: "/goods",
    method: "post",
    data
  });
}
export function updateGoods(data, id) {
  return request({
    url: "/goods/" + id,
    method: "post",
    data
  });
}
export function deleteGoods(id) {
  return request({
    url: "/goods/" + id,
    method: "delete"
  });
}
