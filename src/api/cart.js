import request from "@/utils/request";

export function fetchList() {
  return request({
    url: "/cart",
    method: "get"
  });
}

export function createItem(data) {
  return request({
    url: "/cart",
    method: "post",
    data
  });
}
export function updateCount(id, count) {
  return request({
    url: "/cart/" + id + "/updateCount",
    method: "patch",
    data: {
      count
    }
  });
}
export function deleteItem(id) {
  return request({
    url: "/cart/" + id,
    method: "delete"
  });
}
