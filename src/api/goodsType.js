import request from "@/utils/request";

export function fetchSelectTree(is_parent) {
  return request({
    url: "/goodsType/getSelectTree",
    method: "get",
    params: {
      parent: is_parent
    }
  });
}

export function fetchList(params) {
  return request({
    url: "/goodsType",
    method: "get",
    params
  });
}
export function createGoodsType(data) {
  return request({
    url: "/goodsType",
    method: "post",
    data
  });
}
export function updateGoodsType(data, id) {
  return request({
    url: "/goodsType/" + id,
    method: "post",
    data
  });
}
export function setStatus(id) {
  return request({
    url: "/goodsType/" + id + "/setStatus",
    method: "patch"
  });
}
export function deleteGoodsType(id) {
  return request({
    url: "/goodsType/" + id,
    method: "delete"
  });
}
