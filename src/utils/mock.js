import Mock from "mockjs";
import CartApi from "@/mocks/cart";
Mock.XHR.prototype.proxy_send = Mock.XHR.prototype.send;
Mock.XHR.prototype.send = function() {
  if (this.custom.xhr) {
    this.custom.xhr.withCredetials = this.withCredetials || false;
  }
  this.proxy_send(...arguments);
};
Mock.mock(/\/cart/, "get", CartApi.fetchList);
Mock.mock(/\/cart/, "post", CartApi.createItem);
Mock.mock(/\/cart\/\d\/updateCount/, "patch", CartApi.updateCount);
Mock.mock(/\/cart\/\d/, "delete", CartApi.deleteItem);

export default Mock;
