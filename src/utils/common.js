export default {
  //英文标题按单词截取(参数说明 text:要截取的英文 len：要截取的长度)
  sliceEnglish(text, len, placeholder) {
    //如果要截取文本的长度小于或者等于要截取的长度，则不进行截取，直接返回文本
    if (text) {
      return "";
    }
    if (text.length < len) {
      return text;
    }
    //文本的长度大于要截取的长度，进行截取
    else {
      text = text.substr(0, len);
      //       以空格切分字符串
      var textArr = text.split(" ");
      //           最后一个字符长度
      var lastLen = textArr.pop().length;
      if (lastLen > 3) {
        return text.substr(0, text.length - lastLen - 1) + placeholder;
      } else if (lastLen === 3) {
        return text;
      } else {
        var lastTwoLen = textArr[textArr.length - 1].length;
        return (
          text.substr(0, text.length - lastLen - lastTwoLen - 2) + placeholder
        );
      }
    }
  },
  formatMoney(number, places, symbol, thousand, decimal) {
    /**
     * 金钱格式化
     */
    number = number || 0;
    places = !isNaN((places = Math.abs(places))) ? places : 2;
    symbol = symbol !== undefined ? symbol : "￥";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
      i = parseInt((number = Math.abs(+number || 0).toFixed(places)), 10) + "",
      j = (j = i.length) > 3 ? j % 3 : 0;
    return (
      symbol +
      negative +
      (j ? i.substr(0, j) + thousand : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) +
      (places
        ? decimal +
          Math.abs(number - i)
            .toFixed(places)
            .slice(2)
        : "")
    );
  },
  validateExtend(error, form) {
    /**
     * 验证扩展
     */
    let response = error.response;
    let status = response.status;
    if (status === 422) {
      form.setFields({
        ...error.errorBag
      });
    }
  },
  getBase64(img, callback) {
    /**
     * base64编码
     */
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  },
  getCookie(name) {
    var v = window.document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
    return v ? v[2] : null;
  },
  setCookie(name, value, days) {
    var d = new Date();

    d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);

    window.document.cookie =
      name + "=" + value + ";path=/;expires=" + d.toGMTString();
  },
  deleteCookie(name) {
    this.set(name, "", -1);
  }
};
